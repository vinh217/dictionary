import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gradients/flutter_gradients.dart';

void main() => runApp(new MyApp());

final appTitle = 'Idioms and Slang Dictionary';
final title = 'Idioms and Slang\nDictionary';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        children: <Widget>[
          bodyContent,
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: bottomNavigationBar,
          ),
          searchBar,
        ],
      ),
    );
  }
}

Widget get bodyContent {
  return new Container(
    decoration: BoxDecoration(
      color: Colors.blue,
      gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff102844),
            Color(0xff050B11),
          ]),
    ),
    child: new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        new Row(
          crossAxisAlignment: CrossAxisAlignment.baseline,
          children: [
            new Container(
              margin: EdgeInsets.only(left: 24.0, top: 59.0),
              child: new Text(title,
                  style: TextStyle(fontSize: 25.0, color: Colors.white)),
            ),
            new Container(
                margin: EdgeInsets.only(
                  top: 28.0,
                ),
                child: new Image.asset('assets/images/sign-post.png'))
          ],
        ),
        Container(
          margin: EdgeInsets.only(left: 24,right: 23,top: 16),
          child: new Text('Doloremque, aliquet eius quisquam, quia mauris nec inceptos aliquid curae,'
              ' semper inceptos aptent mattis pretium maecenas, ullamco culpa iaculis fugiat,'
              ' eleifend ipsa praesentium, lacus, blanditiis dignissim quis fuga aut potenti montes nisi dolorem fugit, wisi, aliquid.',
          style: TextStyle(
            color: Color(0xff828282),
            fontSize: 14,
            fontWeight: FontWeight.w500
          ),),),
        Container(
          margin: EdgeInsets.only(left: 24,right: 23,top: 24),
          child: Expanded(
            child: SizedBox(
              height: 270,
              child: ListView.builder(
                  itemBuilder: (context,index) {
                    return GestureDetector(
                      onTap: (){},
                      child: Padding(
                        padding: const EdgeInsets.only(top:6,left: 10,bottom: 6),
                        child: Text('Slang of ${index}', style: TextStyle(fontSize: 14, color: Color(0xff5F9DDA),fontWeight: FontWeight.w500,)),
                      ),
                    );
                  },itemCount: 50,),
            ),
          ),
        )
      ],
    ),
  );
}

Widget get searchBar {
  return Container(
    margin: EdgeInsets.only(left: 24, right: 23, top: 229),
    padding: EdgeInsets.all(8.0),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Color(0xff040D16),
        boxShadow: [
          BoxShadow(
            color: Colors.white,
            offset: Offset.zero,
            blurRadius: 10,
            spreadRadius: 1,
          )
        ]),
    child: TextField(
      style: TextStyle(
        fontSize: 20.0,
        color: Colors.white,
      ),
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Type word here',
        hintStyle: TextStyle(
          fontSize: 20.0,
          color: Colors.white,
        ),
        prefixIcon: Icon(Icons.search, color: Colors.yellowAccent),
      ),
    ),
  );
}

Widget get bottomNavigationBar {
  return Container(
    height: 100,
    decoration: BoxDecoration(boxShadow: [
      BoxShadow(
        color: Colors.white,
        offset: Offset.zero,
        blurRadius: 10,
        spreadRadius: 1,
      )
    ]),
    child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        child: BottomNavigationBar(
            backgroundColor: Color(0xff040D16),
            items: [
              BottomNavigationBarItem(
                icon: Image.asset('assets/images/group02.png'),
                title: Text(''),
              ),
              BottomNavigationBarItem(
                icon: Image.asset('assets/images/group07.png'),
                title: Text(''),
              ),
              BottomNavigationBarItem(
                icon: Image.asset('assets/images/group08.png'),
                title: Text(''),
              )
            ])),
  );
}

// class listDict extends StatelessWidget{
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Container(
//       margin: EdgeInsets.only(top: 10),
//       decoration: BoxDecoration(
//         color: Colors.red,
//       ),
//       child: ListView.builder(
//           itemBuilder: (context,index) {
//             return Text(
//                 'item',
//             style: TextStyle(color: Colors.white),);
//           }
//       ),
//     );
//   }
// }
